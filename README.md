## 🧐 About

It just does one thing. Executes SOQL queries in Salesforce and writes the results to CSV files.

## 🏁 Getting Started

Simply pull down this repo.

### Prerequisites

There are no dependencies other than Python3.x
https://www.python.org/downloads/

### Installing and Setup

1. Open the `environments.py` file in a text editor
2. Ensure that the environment `names` and `base_baths` are correct
3. Make changes, additions or deleted as nessasarry.
4. Save and close the file.

## 🎈 Usage

### Setting up a query ###
Open `main.py` in a text editor and edit the first few lines...

```python
# YOU CAN CHANGE THIS STUFF
# =========================
ENV = Environment.PROD
QUERY = "select Id from log__c where log_name__c like 'Autofi_post%'"
SESSION_ID  = "00D6A000001WSZt..."
API_VERSION = "45.0"
INCLUDE_DELETED_RECORDS = False
```

#### ENV ####

Use the `.` notation to select the environment you want to use. For example, if you want to query
Production, you would use `Environment.PROD`, for QA, you would use `Environment.QA` and so on.

#### QUERY ####

Change this value to any valid SOQL query. Note that nested queries and aggregate queries are not fully yet supported.
Use this at your own risk.

#### SESSION_ID ####

There are several ways to get a session id. One way is through the SFDX CLI tool. If you know what that is, chances are
that you already know how to get a session id.

The other way is by logging into Salesforce in your browser. Be sure you are logging into the same environment that you
selected for the `ENV` variable. I will give you the instructions for Chrome, but it should be pretty much the same in and modern browser.

1. Login to Salesforce
2. Open your develop tools panel (https://developer.chrome.com/docs/devtools/open/)
3. Find and click on the "Application" tab at the top of the developer tools panel
4. In the left sidebar of the Application tab, find and click on "Storage > Cookies" to expand that section
5. You should see an item in the sidebar which has the same name as the domain name you see in your browser's address bar
6. Click on that item and look for a cookie called `sid`. Note, there may be more than one. Clicking the `Name` column header in the table will sort them by name
7. Click in the first `sid` cookie you see in the list. The value of that cookie should be displayed at the bottom.
8. Replace the value of the `SESSION_ID` variable in `main.py` with this value
9. If you get an error message when you run the code stating `Session expired or invalid`, then try the next `sid` in the cookie list
#### API_VERSION ####

You should not have to change this often, but its here if you want to.
#### INCLUDE_DELETED_RECORDS ####

This will force the script to use the `queryall` resource so items marked as `isDeleted == true` or `isArchived == true` will be returned

### Running your query ###

From a terminal navigate to wherever you cloned/downloaded this project
```bash
# for example
cd Documents/Projects/query-salesforce
```

run `main.py`
```bash
python3 main.py
```

That's it!

The result of your query should show up in a newly created folder in the same directory as the `main.py` script