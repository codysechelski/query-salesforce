import subprocess
import json as json

class LoginUsingCli:
    def get_session_id(self, alias):
        result = subprocess.run(['sfdx', 'force:org:open', '-u', alias, '-r', '--json'], stdout=subprocess.PIPE)
        result_out = json.loads(result.stdout)
        return result_out['result']['url'].split('sid=')[1]