import os
import csv
from datetime import datetime

class SfResponseToCsv:

    def write_records_to_csv(self, records, path):
        headers = list(records[0].keys())
        if "attributes" in headers:
            headers.remove("attributes")

        with open(os.path.join(path, "records.csv"), "w") as csv_file:
            csv_writer = csv.DictWriter(csv_file, fieldnames=headers)
            csv_writer.writeheader()
            for record in records:
                del record["attributes"]
                csv_writer.writerow(record)


    def write_info(self, info, path):
        with open(os.path.join(path, "info.txt"), "w") as info_file:
            for key in info:
                info_file.write("{} = {}\n".format(key, info[key]))

    def make_output_folder(self, env_name, obj_type):
        path = "OUTPUT_-_{0:%Y-%m-%d_%I:%M:%S_%p}_-_{1}_-_{2}".format(
            datetime.now(),
            env_name.upper(),
            obj_type.upper()
        )
        os.mkdir(path)
        return path

    def write_results(self, info, records):
        if records and len(records) > 0:
            object_type = records[0]["attributes"]["type"]
            info["object"] = object_type
            info["record_count"] = len(records)
            path = self.make_output_folder(info["environment"],object_type)

            self.write_records_to_csv(records, path)
            self.write_info(info, path)
        else:
            print("0 records returned")