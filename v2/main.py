from query_sf import SfQuery
from sf_response_to_csv import SfResponseToCsv
from environments import Environment
from datetime import datetime
from sf_cli_login import LoginUsingCli
from sf_api import QueryApi

# YOU CAN CHANGE THIS STUFF
# =========================
ENV = Environment.PROD
QUERY = "select Id, Subject from Task where sendgrid_message_id__c != ''"
QUERY_API = QueryApi.BULK_2_0_API_QUERY

AUTH_WITH_CLI = True
SESSION_ID  = "" #Ignore if AUTH_WITH_CLI = True
API_VERSION = "52.0"
INCLUDE_DELETED_RECORDS = False
SILENT_MODE = False


# ONLY CHANGE STUFF BELOW THIS LINE IF YOU KNOW WHAT YOU ARE DOING
# ================================================================
if AUTH_WITH_CLI:
    cli = LoginUsingCli()
    SESSION_ID = cli.get_session_id(ENV["cli_alias"])

query_tools = SfQuery(API_VERSION, SESSION_ID, ENV["base_path"], SILENT_MODE)

if QUERY_API == QueryApi.BULK_2_0_API_QUERY:
    print("Using Bulk 2.0 API")
    query_tools.query_bulk(QUERY, ENV["name"], INCLUDE_DELETED_RECORDS)
    print('DONE')
elif QUERY_API == QueryApi.S_OBJECT_API_QUERY:
    print("Using sObject API")
    records = query_tools.query(QUERY, INCLUDE_DELETED_RECORDS)
    csv_tools = SfResponseToCsv()

    info = {
        "query": QUERY,
        "environment": ENV["name"],
        "date": datetime.now()
    }

    csv_tools.write_results(info, records)
    print('DONE')
elif QUERY_API == QueryApi.TOOLING_API_QUERY:
    print("Method Not Yet Implemented")
    raise NotImplementedError