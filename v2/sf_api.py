from enum import Enum, auto
class QueryApi(Enum):
    S_OBJECT_API_QUERY = auto()
    BULK_2_0_API_QUERY = auto()
    TOOLING_API_QUERY = auto()