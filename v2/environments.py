class Environment:
    DEV = {
        "name" : "Dev",
        "cli_alias" : "dev",
        "base_path" : "https://vroom--dev.my.salesforce.com"
    }

    QA = {
        "name" : "QA",
        "cli_alias" : "vroom-qa",
        "base_path" : "https://vroom--partial.my.salesforce.com"
    }

    UAT = {
        "name" : "UAT",
        "cli_alias" : "vroom-uat",
        "base_path" : "https://vroom--full.my.salesforce.com"
    }

    PROD = {
        "name" : "Production",
        "cli_alias" : "prod",
        "base_path" : "https://vroom.my.salesforce.com"
    }