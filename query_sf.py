from datetime import datetime
import json
import os
from time import sleep
import requests
from sf_response_to_csv import SfResponseToCsv
class SfQuery:
  sf_api_version = ""
  sf_session_id = ""
  sf_base_path = ""
  use_silent_mode = True

  def __init__(self, api_version, session_id, base_path, silent_mode):
    self.sf_api_version = api_version
    self.sf_session_id = session_id
    self.sf_base_path = base_path
    self.use_silent_mode = silent_mode
    print("Silent Mode = {}".format(self.use_silent_mode))

  def query(self, query_string, include_deleted_records = False):
    startTime = datetime.now()
    print('Started at {}'.format(str(startTime)))
    print()
    print('Running Query: {} ...'.format(query_string))
    print()

    endpoint = "{}/services/data/v{}/{}?q={}".format(
      self.sf_base_path,
      self.sf_api_version,
      'queryall' if include_deleted_records else 'query',
      query_string.replace(' ', '+')
    )

    headers = {
      'Authorization': "Bearer {}".format(self.sf_session_id)
    }

    done = False
    req = None
    records = []
    has_errors = False
    error_code = ''
    error_message = ''
    counter = 1

    while not done:
      if not self.use_silent_mode:
        print("sending request number {}".format(counter))

      req = requests.get(endpoint, headers = headers)
      if not self.use_silent_mode:
        print("response number {} received".format(counter))
        print()

      data = req.json()
      if req.status_code == 200:
        if not data['done']:
          endpoint = "{}{}".format(
            self.sf_base_path,
            data['nextRecordsUrl']
          )
          records.extend(data['records'])
          done = data['done']
        else:
          records.extend(data['records'])
          done = True
      else:
        error_code = str(req.status_code)
        error_message = data[0]['message']
        has_errors = True
        done = True

      if not self.use_silent_mode:
        print("{} records so far".format(len(records)))
      counter += 1

    endTime = datetime.now()
    print('Finished at {}'.format(str(endTime)))
    if has_errors:
      raise RuntimeError("ERROR - HTTP CODE: {}\nERROR MESSAGE: {}".format(
        error_code,
        error_message
      ))
    else:
      return records

  def query_bulk(self, query_string, env_name, include_deleted_records = False):
    print()
    print('Running Query: {} ...'.format(query_string))
    print()
    endpoint = "{}/services/data/v{}/jobs/query".format(
      self.sf_base_path,
      self.sf_api_version,
    )

    headers = {
      "Authorization": "Bearer {}".format(self.sf_session_id),
      "Content-Type": "application/json",
      "charset": "UTF-8"
    }

    body = {
      "operation": "queryAll" if include_deleted_records else "query",
      "query": query_string,
      "contentType" : "CSV",
    }

    done = False
    jobId = None
    has_errors = False
    error_code = ''
    error_message = ''
    queried_object = ''
    total_record_count = 0
    poling_freq = 5 #seconds

    # Create the job
    create_job_resp = requests.post(endpoint, headers=headers, data=json.dumps(body))
    if create_job_resp.status_code == 200:
      jobId = create_job_resp.json()['id']
      print("Bulk Job ID = {}".format(jobId))
    else:
      error_code = str(create_job_resp.status_code)
      error_message = create_job_resp.json()[0]['message']
      has_errors = True
      done = True

    if has_errors:
      raise RuntimeError("ERROR - HTTP CODE: {}\nERROR MESSAGE: {}".format(
        error_code,
        error_message
      ))
    else:
      # Poll for the status of the job
      while not done:
        endpoint = "{}/services/data/v{}/jobs/query/{}".format(
          self.sf_base_path,
          self.sf_api_version,
          jobId
        )

        headers = {
          "Authorization": "Bearer {}".format(self.sf_session_id),
          "Content-Type": "application/json",
          "charset": "UTF-8"
        }
        check_status_resp = requests.get(endpoint, headers=headers)
        if check_status_resp.status_code == 200:
          if not self.use_silent_mode:
            print("Bulk Job ID = {}".format(jobId))
            print("Status: {}".format(check_status_resp.json()["state"]))
            if "numberRecordsProcessed" in  check_status_resp.json():
              print("Records Processed: {}".format(check_status_resp.json()["numberRecordsProcessed"]))
              queried_object = check_status_resp.json()["object"]
            print("checking again in {} seconds...".format(poling_freq))
            print()
          done = check_status_resp.json()["state"] == "JobComplete" or check_status_resp.json()["state"] == "Failed"
          if check_status_resp.json()["state"] == "Failed":
            error_code = None
            error_message = check_status_resp.json()["errorMessage"]
            has_errors = True
          if done:
            total_record_count = check_status_resp.json()["numberRecordsProcessed"]
          else:
            sleep(poling_freq)
        else:
          error_code = str(create_job_resp.status_code)
          error_message = create_job_resp.json()[0]['message']
          has_errors = True
          done = True

      if has_errors:
        raise RuntimeError("ERROR - HTTP CODE: {}\nERROR MESSAGE: {}".format(
          error_code,
          error_message
        ))
      else:
        #Download the file
        print("writing csv file ...")
        csv_tools = SfResponseToCsv()
        path = csv_tools.make_output_folder(env_name, queried_object)

        endpoint = "{}/services/data/v{}/jobs/query/{}/results?maxRecords={}".format(
          self.sf_base_path,
          self.sf_api_version,
          jobId,
          total_record_count
        )

        headers = {
          "Authorization": "Bearer {}".format(self.sf_session_id),
          "Content-Type": "application/json",
          "charset": "UTF-8"
        }
        with requests.get(endpoint, headers=headers, stream=True) as r:
          r.raise_for_status()
          with open(os.path.join(path, "records.csv"), "wb") as f:
            for chunk in r.iter_content(chunk_size=8192):
              f.write(chunk)

        info = {
          "query": query_string,
          "bulk_job_id": jobId,
          "environment": env_name,
          "date": datetime.now(),
          "object": queried_object,
          "record_count": total_record_count
        }

        csv_tools.write_info(info, path)